A partir del esquema de redes e ips del aula:

                                                    +------------ internet
                                                    |
                                                +-------+
                       +------------------------+ PROFE +---------------------+
                       |                        +-------+                     |
      2.1.1.0/24       |.254                                 2.1.2.0/24       |.254
      +-----------------------------------+                  +-----------------------------------+
            |.6        |.5        |.4                              |.3        |.2        |.1
            |          |          |                                |          |          |
         +-----+    +-----+    +-----+                          +-----+    +-----+    +-----+
         |PC06 |    |PC05 |    |PC04 |                          |PC03 |    |PC02 |    |PC01 |
         +-----+    +-----+    +-----+                          +-----+    +-----+    +-----+
                       |                                                      |
      2.2.1.0/24       |.254                                 2.2.2.0/24       |.254
      +-----------------------------------+                  +-----------------------------------+
            |.12       |.11       |.10                             |.9        |.8        |.7
            |          |          |                                |          |          |
         +-----+    +-----+    +-----+                          +-----+    +-----+    +-----+
         |PC12 |    |PC11 |    |PC10 |                          |PC09 |    |PC08 |    |PC07 |
         +-----+    +-----+    +-----+                          +-----+    +-----+    +-----+
                       |                                                      |
      2.3.1.0/24       |.254                                 2.3.2.0/24       |.254
      +-----------------------------------+                  +-----------------------------------+
            |.18       |.17       |.16                             |.15       |.14       |.13
            |          |          |                                |          |          |
         +-----+    +-----+    +-----+                          +-----+    +-----+    +-----+
         |PC18 |    |PC17 |    |PC16 |                          |PC15 |    |PC14 |    |PC13 |
         +-----+    +-----+    +-----+                          +-----+    +-----+    +-----+
                       |                                                      |
      2.4.1.0/24       |.254                                 2.4.2.0/24       |.254
      +-----------------------------------+                  +-----------------------------------+
            |.24       |.23       |.22                             |.21       |.20       |.19
            |          |          |                                |          |          |
         +-----+    +-----+    +-----+                          +-----+    +-----+    +-----+
         |PC24 |    |PC23 |    |PC22 |                          |PC21 |    |PC20 |    |PC19 |
         +-----+    +-----+    +-----+                          +-----+    +-----+    +-----+
                       |                                                      |
      2.5.1.0/24       |.254                                 2.5.2.0/24       |.254
      +-----------------------------------+                  +-----------------------------------+
            |.30       |.29       |.28                             |.27       |.26       |.25
            |          |          |                                |          |          |
         +-----+    +-----+    +-----+                          +-----+    +-----+    +-----+
         |PC30 |    |PC29 |    |PC28 |                          |PC27 |    |PC26 |    |PC25 |
         +-----+    +-----+    +-----+                          +-----+    +-----+    +-----+
         
1. Identifica cual es tu PC, que ips/máscaras ha de llevar sobre qué interfaces
	- Mi ordenador es el PC07
	- Tiene asignada la IP 2.2.2.7 con mascara 255.255.255.0 en la interfaz enp2s0
         
1. Lista de órdenes para poder introducir manualmente las direcciones ip de tu puesto de trabajo con la orden ip y evitar que el NetworkManager o configuraciones previas interfieran en estas configuraciones
	- su - ----------------------------> entro en root para poder realizar las ordenes
	- Systemctl stop NetworkManager----> Matamos el network manager para que no nos cambie la ip sin desearlo
	- ip a f enp2s0 -------------------> Quitamos todas las ip actuales de la tarjeta enp2s0
	- ip a a 2.2.2.7/24 dev enp2s0-----> Le añadimos la nueva IP con mascara /24
	- ip r a default via 2.2.2.8/24----> Le añado la ruta para salir a traves del router de mi fila

1. Lista de órdenes para configurar el PC de tu fila como router con un comentario explicando lo que hace cada línea
	- su -   ----------------------------> entro en root para poder realizar las ordenes
	- Systemctl stop NetworkManager------> Matamos el network manager para que no nos cambie la ip sin desearlo
	- ip a f enp2s0 ---------------------> Quitamos todas las ip actuales de la tarjeta enp2s0
	- ip a a 2.2.2.8/24 dev enp2s0-------> Le añadimos la nueva IP con mascara /24 de mi fila
	- ip a a 2.3.2.254/24 dev enp2s0-----> Le añadimos la nueva IP con mascara /24 de la fila de detras
	- ip r a default via 2.2.2.254----> Le añado la ruta para salir a internet a través del router de la fila de delante conectado a mi red
	- ip r a 2.3.2.0/24 via 2.3.2.14/24--> Le añado la ruta para salir a las filas de atras a través del router de la fila de atras a la que estoy conectado
	- echo 1 > /proc/sys/net/ipv4/ip_forward

1. Explicación de cada línea de la salida del comando ip route show
	- [root@j07 ~]# ip route show
	- default via 2.2.2.254 dev enp2s0 -----> Salgo a internet a través de la ip 2.2.2.254 (ordenador fila delante conectado a mi red)
	- 2.2.2.0/24 dev enp2s0  proto kernel  scope link  src 2.2.2.8 
	- 2.3.2.0 via 2.3.2.14 dev enp2s0-------> Salgo a la fila de atrás través de mi ip 2.3.2.14(ordenador fila detrás al que estoy conectado)
	- 2.3.2.0/24 dev enp2s0  proto kernel  scope link  src 2.3.2.254 

1. Hacer un ping a un pc de tu misma fila y al PC19 y al PC24. 

**Capturar los paquetes con wireshark y guardar la captura. Analizarla 
observando el campo TTL del protocolo IP. Explicar los diferentes valores
de ese campo en función de la ip a la que se ha hecho el ping**

1. Hacer un traceroute y un mtr al 8.8.8.8 Explicar a partir de que ip se corta y por qué. 

1. Explica que es el enmascaramiento y cómo se aplicaría en el ordenador del profe para que de salida a internet
