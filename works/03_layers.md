#### 1. ip a

Borra todas las rutas y direcciones ip, para el servicio NetworkManager y asegurate que no queda ningun demonio de dhclient corriendo. Comprueba que no queda ninguna con "ip a" y "ip r"
```
ip a f enp2s0
ip r f all
systemctl stop NetworkManager
dhclient -r
```
 
Ponte las siguientes ips en tu tarjeta ethernet: 
2.2.2.2/24, 
3.3.3.3/16, 
4.4.4.4/25,

```
ip a a 2.2.2.2/24 dev enp2s0
ip a a 3.3.3.3/16 dev enp2s0
ip a a 4.4.4.4/25 dev enp2s0
ip a
```
```
    inet 2.2.2.2/24 scope global enp2s0
       valid_lft forever preferred_lft forever
    inet 3.3.3.3/16 scope global enp2s0
       valid_lft forever preferred_lft forever
    inet 4.4.4.4/25 scope global enp2s0
       valid_lft forever preferred_lft forever
```

Consulta la tabla de rutas de tu equipo
```
ip r
2.2.2.0/24 dev enp2s0  proto kernel  scope link  src 2.2.2.2 
3.3.0.0/16 dev enp2s0  proto kernel  scope link  src 3.3.3.3 
4.4.4.0/25 dev enp2s0  proto kernel  scope link  src 4.4.4.4
```
Haz ping a las siguientes direcciones y justifica por quÃ© en algunas sale el mensaje de "Network is unrecheable", en otras contesta y en otras se queda esperando sin dar mensajes de error:

2.2.2.2 , 2.2.2.254 , 2.2.5.2 , 3.3.3.35 , 3.3.200.45 , 4.4.4.8, 4.4.4.132

- Cuando nos da network is unreachable es porque nosotros estamos en una red distinta a la introducida.
- Cuando no nos responde o nos hace el ping, la direccion de destino esta en nuestra red. 

#### 2. ip link

Borra todas las rutas y direcciones ip de la tarjeta ethernet
```
ip a f enp2s0
ip r f all
```
Conecta una segunda interfaz de red por el puerto usb

Cambiale el nombre a usb0
```
ip link set [nombre tarjeta] down
ip link set [no,bre tarjeta] name usb0
ip link set usb0 up
```

Modifica la direccion MAC
```
ip link set usb0 down
ip link set usb0 address 00:11:22:33:44:55
ip link set usb0 up
```

Asi­gnale la direccion ip 5.5.5.5/24 a usb0 y 7.7.7.7/24 a la tarjeta de la placa base.
```
ip a a 5.5.5.5/24 dev usb0
ip a a 7.7.7.7/24 dev enp2s0
```
Observa la tabla de rutas
```
ip r show
5.5.5.0/24 dev usb0  proto kernel  scope link  src 5.5.5.5 linkdown 
7.7.7.0/24 dev enp2s0  proto kernel  scope link  src 7.7.7.7
```

#### 3. iperf

Borra todas las rutas y direcciones ip de la tarjeta ethernet
```
ip a f enp2s0
```

En cada ordenador os ponÃ©is la ip 172.16.99.XX/24 (XX=puesto de trabajo)
```
ip a a 172.16.99.07/24 dev enp2s0
```

Lanzar iperf en modo servidor en cada ordenador
```
iperf -s
------------------------------------------------------------
Server listening on TCP port 5001
TCP window size: 85.3 KByte (default)
------------------------------------------------------------
```

Comprueba con netstat en quÃ© puerto escucha
```
netstat -utlnp
Active Internet connections (only servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name    
tcp        0      0 0.0.0.0:5001            0.0.0.0:*               LISTEN      3989/iperf 
```
Conectarse desde otro pc como cliente
```
iperf -t 2 -c 172.16.99.07
```
Repetir el procedimiento y capturar los 30 primeros paquetes con tshark
```
ficher out.pcap
```
Encontrar los 3 paquetes del handshake de tcp
```
1	0.000000000	172.16.99.8	172.16.99.7	TCP	1514	50410→5001 [ACK] Seq=1 Ack=1 Win=229 Len=1448 TSval=3059689 TSecr=2899689
2	0.000034673	172.16.99.7	172.16.99.8	TCP	66	5001→50410 [ACK] Seq=1 Ack=1449 Win=1720 Len=0 TSval=2899691 TSecr=3059689
3	0.000115912	172.16.99.8	172.16.99.7	TCP	1514	50410→5001 [ACK] Seq=1449 Ack=1 Win=229 Len=1448 TSval=3059689 TSecr=2899689
```
Abrir dos servidores en dos puertos distintos

Observar como quedan esos puertos abiertos con netstat

Conectarse al servidor con dos clientes y que la prueba dure 1 minuto

Mientras tanto con netstat mirar conexiones abiertas

