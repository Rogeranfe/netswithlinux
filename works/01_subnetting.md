# Exercicis de Subxarxes

##### Instruccions generals:
1. Alerta, feu tots els càlculs sense calculadora!
2. Si necessiteu fer càlculs en un full apart, si us plau, indiqueu-m'ho en cada exercici i entregueu-me aquest full.
3. En total hi ha 5 exercicis (reviseu totes les pàgines)

##### 1. Donades les següents adreces IP, completa la següent taula (indica les adreces de xarxa i de broadcast tant en format decimal com en binari).


IP | Adreça de Xarxa | Adreça de Broadcast Extern | Rang IPs per a Dispositius 
---|---|---|---
172.16.4.23/25 | 172.16.4.0 | 172.16.4.127 | 172.16.4.172.1 - 172.16.4.126
174.187.55.6/23 | 174.187.54.0 | 174.187.55.254 | 174.187.54.1 - 174.187.55.255
10.0.25.253/18 | 10.0.0.0 | 10.0.63.254 | 10.0.0.1 - 10.0.63.255
209.165.201.30/27 | 209.165.201.0 | 209.165.201.31 | 209.165.201.1 - 209.165.201.30


##### 2. Donada la xarxa 172.28.0.0/16, calcula la màscara de xarxa que necessites per poder fer 80 subxarxes. Tingues en compte que cada subxarxa ha de tenir capacitat per a 400 dispositius. Indica la màscara de xarxa calculada tant en format decimal com en binari.

+ Amb /23 podré tenir fins a 510 hosts per subxarxa, ja que:
+ 32 bits totals - 23 = 9 bits per a hosts
+ 2⁹ = 512 
+ 512 - 2 reservades = 510 hosts per xarxa
+ Dels /23 al /16 em queden 7 bits lliures per fer subxarxes
+ 2⁷ = 128 subxarxes

- Mascara binari: 11111111.11111111.11111110.00000000
- Mascara decimal: 255.255.254.0

##### 3. Donada la xarxa 10.192.0.0
a) Calcula la màscara de xarxa que necessites per poder adreçar 1500 dispositius. Indica-la tant en format decimal com en binari.

+ 2¹¹ son 2048 hosts
+ 32 bits totals -11 per hosts = 21
+ Tindre una xarxa /21
+ La mascara será 11111111.11111111.11111000.00000000 --> 255.255.248.0


b) Si fixem l'adreça de la xarxa mare a la 10.192.0.0/10, quantes subxarxes de 1500 dispositius podrem fer?

+ Amb /21 podre tenit fins a 2046 hosts per xarxa ja que:
+ 32 bits - 21 = 11 bits per a hosts
+ 2¹¹ son 2048 hosts
+ 2048 hosts - 2 reservades = 2046
+ Del /21 al /10 em queden 11 bits lliures per a xarxes
+ 2¹¹ = 2048 subxarxes


c) Si ara considerem que l'adreça de xarxa mare és la 10.192.0.0 amb la màscara de xarxa calculada a l'apartat (a), calcula la nova màscara de xarxa que permeti fer 3 subxarxes de 500 dispositius cadascuna. Indica-la tant en format decimal com en binari.

- Tenim 10.192.0.0/21
- amb 2⁹=512 combinacions
- 32 bits -9 bits hosts ---> /23 
- del /23 al /21 em queden 2 bits lliures per a subxarxes
- 2² = 4 subxarxes
- Per tant podré fer una total de 4 subxarxes de 510 hosts cadascuna

+ La máscara será 11111111.11111111. 1111111    0.00000000
+ En decimal és 255.255.254.0

##### 4. Donada la xarxa 10.128.0.0 i sabent que s'ha d'adreçar un total de 3250 dispositius
a) Calcula la màscara de xarxa per crear una adreça de xarxa mare que permeti adreçar tots els dispositius indicats. Indica-la tant en format decimal com en binari.

+ Amb un /20 podre tenir fins a 4094 hosts ja que:
+ 2¹² = 4096 combinacions
+ 4096 combinacions - 2 adreces reservades = 4094 hosts
+ Al tenir una xarxa /20 la máscara será:
+ 11111111.11111111.1111 0000.00000000
+ 255.255.240.0

b) Calcula la nova màscara de xarxa que permeti fer 5 subxarxes amb 650 dispositius cadascuna d'elles. Indica-la tant en format decimal com en binari.

10.128.0.0/20
- 32 bits - 22 = 10 bits per a hosts
- Amb 2¹⁰ podre tenir fins a 1024 combinacions
- Pero del /22 al /20 nomes em queden 2 bits per a subxarxes i només podre fer un maxim de 4 en comptes de les 5 demanades.

c) Comprova que, realment, cada subxarxa pot adreçar els 650 dispositius demanats. En cas que no sigui així, quina és la capacitat màxima de dispositius de cadascuna d'aquestes xarxes? Indica la fórmula que fas anar per calcular aquesta dada.

+ Per poder fer 5 subxarxes hauré de partir de una xarxa /19, ja que si no només em quedarien 2 bits per a subxarxes i només podria tenir 4 subxarxes en comptes de les 5 demanades

- Amb un /22 podre tenir 1024 combinacions ja que:
- 32 bits - 22 = 10 bits per a hosts
- Amb 2¹⁰ podre tenir fins a 1024 combinacions
- 1024 - 2 reservades = 1022 hosts per xarxa
- Del /22 al /19 em queden 3 bits lliures per a subxarxes
- 2³ = 8 subxarxes
- MAC ---> 11111111.11111111.111 00000.00000000
- MAC ---> 255.255.224.0

d) A partir de la xarxa mare que has obtingut amb la màscara de xarxa calculada a l'apartat (a), podem fer dues subxarxes amb 1625 dispositius cadascuna d'elles? Indica els càlculs que necessites fer per raonar la teva resposta.

- 10.128.0.0/20
- Calculem la mascara 11111111.11111111.11111000.00000000
- Si agafem un bit més passant del /20 al /21 tindrem un bit per a subxarxes i podrem fer 2 subxarxes amb una capacitat de 2046 hosts.
##### 5. Donada l'adreça de xarxa mare (AX mare) 172.16.0.0/12, s'han de calcular 6 subxarxes.
a) Quants bits necessites per ampliar la Màscara de Xarxa (MX) per tal de poder fer aquestes 6 subxarxes?
- Necessito 3 

b) Dóna la nova MX, tant en format decimal com en binari.
- 11111111.11111110.00000000.00000000
- 255.254.0.0

c) Per cada subxarxa nova que has de crear, indica
- L'adreça de xarxa, en format decimal i en binari
- L'adreça de broadcast extern, en format decimal i en binari
- El rang d'IPs per a dispositius

Nom| Adreça de Xarxa | Adreça de Broadcast Extern | Rang IPs per a Dispositius
---|---|---|---
Xarxa 1 |172.16.0.0 10101100.00010000.00000000.00000000| 10101100.00010001.11111111.11111111 172.17.255.255|172.16.0.1-172.17.255.254
Xarxa 2 |172.18.0.0 10101100.00010010.00000000.00000000| 10101100.00010011.11111111.11111111 172.19.255.255|172.18.0.1-172.19.255.254
Xarxa 3 |172.20.0.0 10101100.00010100.00000000.00000000| 10101100.00010101.11111111.11111111 172.21.255.255| 172.20.0.1-172.21.255.254
Xarxa 4 |172.22.0.0 10101100.00010110.00000000.00000000| 10101100.00010111.11111111.11111111 172.23.255.255| 172.22.0.1-172.23.255.254
Xarxa 5 |172.24.0.0 10101100.00011000.00000000.00000000| 10101100.00011001.11111111.11111111 172.25.255.255| 172.24.0.1-172.25.255.254
Xarxa 6 |172.26.0.0 10101100.00011010.00000000.00000000| 10101100.00011011.11111111.11111111 172.27.255.255| 172.26.0.1-172.27.255.254


d) Tenint en compte el número de bits que has indicat en l'apartat a), quantes subxarxes podríem fer, realment? Dóna'n la fórmula que s'utilitza per calcular aquesta dada.
- Necessito 3 bits ja que 2³ = 8
- això em permet fer 8 subxarxes

e) Segons la MX que has calculat als apartats a) i b), quants dispositius pot tenir cada subxarxa? Dóna'n la fórmula que s'utilitza per calcular aquesta dada.
- Per poder fer els apartats a i b he hagut de pasar de una /12 a una /15 (afegint els 3 bits per a subxarxes)
- La resta de bits de l'adreça, em serviran per a hosts.
- 32 bits totals -15 = 17 bits
- 2¹⁷ = 131072 combinacions
- 131072 - 2 combinacions reservades = 131070 hosts

##### 6. Donada l'adreça de xarxa mare (AX mare) 192.168.1.0/24, s'han de calcular 4 subxarxes.

a) Quants bits necessites per ampliar la Màscara de Xarxa (MX) per tal de poder fer aquestes 6 subxarxes?
- Necessito 2 bits--> 2² = 4

b) Dóna la nova MX, tant en format decimal com en binari.
- 11111111.11111111.11111111.11000000 -->255.255.255.192

c) Per cada subxarxa nova que has de crear, indica
- L'adreça de xarxa, en format decimal i en binari
- L'adreça de broadcast extern, en format decimal i en binari
- El rang d'IPs per a dispositius

Nom| Adreça de Xarxa | Adreça de Broadcast Extern | Rang IPs per a Dispositius
---|---|---|---
Xarxa 1|192.168.1.0/26 11000000.10101000.00000001.00000000 | 192.168.1.63 | 192.168.1.1-192.168.1.62 
Xarxa 2|192.168.1.64/26 11000000.10101000.00000001.01000000 | 192.168.1.127 | 192.168.1.65-192.168.1.126
Xarxa 3|192.168.1.128/26 11000000.10101000.00000001.10000000| 192.168.1.191 | 192.168.1.129-192.168.1.190
Xarxa 4|192.168.1.192/16 11000000.10101000.00000001.11000000 | 192.168.1.254 | 192.168.1.193-192.168.1.253
