LLOC     = 19
IF_TRUNK = 'eth4'


conf_ifaces="""
/interface vlan add vlan-id={lloc}{xarxa_id} interface={ifname} name=vnet{xarxa_id}
/ip address add interface=vnet{xarxa_id} address=10.1{lloc}.{xarxa_id}.1/24
/ip pool add ranges=10.1{lloc}.{xarxa_id}.100-10.1{lloc}.{xarxa_id}.199 name=pool{xarxa_id} 
/ip dhcp-server network add dns-server=8.8.8.8 gateway=10.1{lloc}.{xarxa_id}.1 \\
    netmask=24 domain=meloinvento.com address=10.1{lloc}.{xarxa_id}.0/24
/ip dhcp-server add address-pool=pool{xarxa_id} interface=vnet{xarxa_id} disabled=no
"""
route = "/ip route add dst-address=10.1{lloc}.0.0/17 gateway=192.168.3.2{lloc}"

for id in range(2,7):
	print(conf_ifaces.format(lloc=LLOC,xarxa_id=id,ifname=IF_TRUNK))
	
for lloc in range(1,36):
	print(route.format(lloc=str(lloc).zfill(2)))
	

	
